
const execSync = require('child_process').execSync;
const fs = require('fs-plus');
const path = require('path');
const semver = require('semver');

const HOST_NAME = 'oliverwymantechssg';
const REPO_NAME = 'ngpd-merceros-testautomation-ta';

(function () {
  console.log('Starting...');
  execSync(`git clone git@bitbucket.org:${HOST_NAME}/${REPO_NAME}.git`);
  let newVersion;

  try {
    // find max tag
    const tags = execSync(`cd ${path.join(process.cwd(), REPO_NAME)} && git tag`)
      .toString()
      .split('\n')
      .map(semver.clean);
    const maxTag = semver.maxSatisfying(tags, '*');

    // increment patch version
    const newTag = semver.inc(maxTag, 'patch');

    if (!newTag) {
      throw new Error('Can\'t increment version');
    }

    // set new version in package.json
    const PATH_TO_PACKAGE = path.join(process.cwd(), REPO_NAME, 'package.json');
    const packageJson = require(PATH_TO_PACKAGE);
    packageJson.version = newTag;
    newVersion = newTag;

    fs.writeFileSync(PATH_TO_PACKAGE, JSON.stringify(packageJson, null, 2), 'utf8');

    // commit and push new version with tag
    execSync(`cd ${path.join(process.cwd(), REPO_NAME)} \
      && git add . \
      && git commit -m "auto-update version" \
      && git tag v${newTag} \
      && git push origin master --tags`);
  } finally {
    fs.removeSync(REPO_NAME);
    console.log(`\nVersion updated! New version is ${newVersion}\n`);
    console.log('\x1b[43m\x1b[30m%s\x1b[0m' ,'Don\'t forget to update version in AWS:');
    console.log(`\
    1. http://jenkins-dev.mercer.com/job/ngpd-merceros-testautomation-ta_aws-sync-repository
    2. https://dev-jenkins.us-east-1.aws.mercer.com/job/ngpd-merceros-testautomation-ta_NPM_release
    3.(optional) https://dev-jenkins.us-east-1.aws.mercer.com/job/ngpd-merceros-testautomation-ta_NPM_CI`);
  }
})();
