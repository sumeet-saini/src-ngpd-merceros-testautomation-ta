/**
 * Multiple Before hooks are executed in the order that they were defined.
 * Multiple After hooks are executed in the reverse order that they were defined.
 */
const path = require('path');
const { Before } = require('cucumber');

Before(function (testCase) {
  // this will define location of current feature dir
  this.testLocation = path.dirname(testCase.sourceLocation.uri);
});
