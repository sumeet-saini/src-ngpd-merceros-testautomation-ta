module.exports = {
  elementHelper: require('./helpers/element-helper.js'),
  fileHelper: require('./helpers/file-helper.js'),
  stringHelper: require('./helpers/string-helper.js'),
  endpointHelper: require('./helpers/endpoint-helper.js'),
  actionHelper: require('./helpers/action-helper'),
  localizationHelper: require('./helpers/localization-helper'),
  actionStepDefinitions: require('./step-definitions/action-step-definitions.js'),
  verificationStepDefinitions: require('./step-definitions/verification-step-definitions.js'),
  screenshotStepDefinitions: require('./step-definitions/screenshot-step-definitions'),
  hooks: require('./support/hooks'),
};
